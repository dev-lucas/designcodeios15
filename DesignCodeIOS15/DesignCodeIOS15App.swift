//
//  DesignCodeIOS15App.swift
//  DesignCodeIOS15
//
//  Created by Lucas Gomes on 22/05/23.
//

import SwiftUI

@main
struct DesignCodeIOS15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
